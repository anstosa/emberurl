from mongoengine import *
import datetime

connect('emberURL')


class User(DynamicDocument):
    firstName = StringField(max_length=50)
    lastName = StringField(max_length=50)
    email = EmailField(required=True)
    createdOn = DateTimeField(default=datetime.datetime.now)
    password = StringField(required=True)
    salt = StringField(required=True)

    def __unicode__(self):
        return '%s %s' % (self.firstName, self.lastName)


class Group(DynamicDocument):
    name = StringField(max_length=50)
    createdOn = DateTimeField(default=datetime.datetime.now)
    createdBy = ReferenceField(User)

    def __unicode__(self):
        return self.name

rankChoice = (('A', 'Admin'),
              ('M', 'Member'))


class GroupMembership(DynamicDocument):
    group = ReferenceField(Group)
    user = ReferenceField(User)
    createdOn = DateTimeField(default=datetime.datetime.now)
    createdBy = ReferenceField(User)
    deletedOn = DateTimeField()
    rank = StringField(max_length=3, choices=rankChoice)

    def __unicode__(self):
        return '%s -> %s' % (self.user, self.group)

documentChoice = (('L', 'Link'),
                  ('S', 'Snippet'))


class Note(DynamicDocument):
    address = StringField(max_length=100, required=True)
    doctype = StringField(max_length=3, choices=documentChoice)
    createdOn = DateTimeField(default=datetime.datetime.now)
    createdBy = ReferenceField(User)

    def __unicode__(self):
        return "Address: %s" % (self.address)


class NoteRevision(DynamicDocument):
    note = ReferenceField(Note)
    createdOn = DateTimeField(default=datetime.datetime.now)
    createdBy = ReferenceField(User)

    def __unicode__(self):
        return "Revision of: %s" % (self.note)


class PermissionAssignment(DynamicDocument):
    group = ReferenceField(Group)
    note = ReferenceField(Note)
    createdOn = DateTimeField(default=datetime.datetime.now)
    createdBy = ReferenceField(User)
    deletedOn = DateTimeField(default=None)
    permissions = ListField(StringField(max_length=100))

    def __unicode__(self):
        return '%s -> %s' % (self.group, self.note)


class Log(DynamicDocument):
    user = ReferenceField(User)
    note = ReferenceField(Note)
    affectedUser = ReferenceField(User)
    group = ReferenceField(Group)
    noteRevision = ReferenceField(NoteRevision)
    logDate = DateTimeField(default=datetime.datetime.now)
    message = StringField()

    def __unicode__(self):
        return '%s: %s' % (note, message)


class ApiKey(DynamicDocument):
    user = ReferenceField(User)
    key = StringField()
    owner = StringField()
    createdOn = DateTimeField(default=datetime.datetime.now)
    deletedOn = DateTimeField(default=None)


class RandomAddress(Document):
    address = StringField()
