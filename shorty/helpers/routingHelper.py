from django.shortcuts import redirect, render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from shorty.helpers import *
from shorty.models import *
from shorty.forms import *
from mongoengine import *
import re


def getSubdomain(request):
    domain = request.META.get('HTTP_HOST')
    splitDomain = domain.split('.')
    if len(splitDomain) is 3:
        return splitDomain[0]
    else:
        return None


def viewURL(urlCode, request):
    # Get the note
    notesList = Note.objects(address=urlCode)
    if len(notesList) is 1:
        note = notesList[0]
    else:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'no note at that address'
        })

    # Verify the user has permission to view this note
    user = getUser(request)
    if not canViewNote(note, user):
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'you do not have permission to view this note'
        })

    # Determine what type of note this is
    if note.doctype == 'L':
        longAddress = note.longAddress
        if longAddress is not None:
            if longAddress[:4] == "http":
                return redirect(longAddress)
            else:
                return redirect("http://%s" % longAddress)
        else:
            return render_to_response('shorty/errorDisplay.html', {
                'error': 'full address missing'
            })
    elif note.doctype == 'S':
        # Get API Key
        apikeyList = ApiKey.objects(user=user, owner="qbf.tv", deleted=None)
        if len(apikeyList) is 1:
            apikey = apikeyList[0].key
        else:
            apikey = createApiKey(user, 'http://qbf.tv')

        return render_to_response('shorty/snippetDisplay.html', {
            'note': note,
            'apikey': apikey
        })
    else:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'type unknown'
        })


def viewURLAdmin(urlCode, request):
    # Check the note exists
    noteList = Note.objects(address=urlCode)
    if len(noteList) is 1:
        note = noteList[0]
    else:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'there is no note at that address'
        })

    # Check the user is logged in
    user = getUser(request)
    if user is None:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'please log in to view the admin panel'
        })

    # Grab and API Key, just to have handy
    apikeyList = ApiKey.objects(user=user, owner="qbf.tv", deleted=None)
    if len(apikeyList) is 1:
        apikey = apikeyList[0].key
    else:
        apikey = createApiKey(user, 'http://qbf.tv')

    if request.method == 'POST':
        newPerm = NewPermission(request.POST)
        if newPerm.is_valid():
            error = False
            result = createPermissionFromForm(request, newPerm, urlCode)
            if result is True:
                return render_to_response('shorty/noteAdmin.html', {
                    'note': note,
                    'message': 'new permission added',
                    'apikey': apikey,
                    'newPermission': NewPermission(),
                    'currentGroups': PermissionAssignment.objects(note=note, deletedOn=None),
                    'username': user
                }, context_instance=RequestContext(request))
            else:
                error = result
        else:
            error = 'invalid form'

        if error is not False:
            return render_to_response('shorty/noteAdmin.html', {
                'note': note,
                'error': error,
                'newPermission': newPerm,
                'apikey': apikey,
                'currentGroups': PermissionAssignment.objects(note=note, deletedOn=None),
                'username': user
            }, context_instance=RequestContext(request))
    else:
        # Check that the user logged in created, or is admin of this note
        if canAdminNote(note, user):
            return render_to_response('shorty/noteAdmin.html', {
                'note': note,
                'apikey': apikey,
                'newPermission': NewPermission(),
                'currentGroups': PermissionAssignment.objects(note=note, deletedOn=None),
                'username': user
            }, context_instance=RequestContext(request))
        else:
            return render_to_response('shorty/errorDisplay.html', {
                'error': 'you do not have permission to manage this note'
            })
