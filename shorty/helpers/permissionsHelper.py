from shorty.helpers import *
from shorty.models import *
from shorty.forms import *
from mongoengine import *
import datetime
import hashlib


def getUser(request, providedEmail=None):
    try:
        email = providedEmail if providedEmail else request.session['userEmail']
        u = User.objects(email=email)
        assert len(u) is 1
        return u[0]
    except:
        return None


def validateLogin(request, form):
    email = form.cleaned_data['email']

    userSearch = User.objects(email=email)
    if len(userSearch) is 1:
        submittedPassword = form.cleaned_data['password']
        savedHash = userSearch[0].password
        savedSalt = userSearch[0].salt

        newHash = hashlib.sha512(submittedPassword + savedSalt)
        if newHash.hexdigest() == savedHash:
            request.session['userEmail'] = email
            return True
    return False


def canViewNote(note, user):
    return notePermissionHelper(note, user, 'R')


def canSaveNote(note, user):
    return notePermissionHelper(note, user, 'W')


def canAdminNote(note, user):
    return notePermissionHelper(note, user, 'A')


def notePermissionHelper(note, user, permType):
    # Check if public has access
    publicGroup = Group.objects(name='public')[0]
    permsList = PermissionAssignment.objects(note=note, deletedOn=None, group=publicGroup)
    if len(permsList) is not 0:
        for perm in permsList:
            if permType in perm.permissions:
                return True

    # Check if there is a user logged in. If not, they have no chance
    if user is None:
        return False

    # Check if this note was created by the user logged in
    if note.createdBy == user:
        return True

    # Check all permission assignments
    permsList = PermissionAssignment.objects(note=note, deletedOn=None)
    for perm in permsList:
        groupsList = GroupMembership.objects(group=perm.group, user=user, deletedOn=None)
        if len(groupsList) is not 0:
            if permType in perm.permissions:
                return True

    # No luck finding a rule granting access
    return False


def deletePermission(note, groupName, deleter):
    # Check that this user has permission to perform this action
    if not canAdminNote(note, deleter):
        return "you do not have permission to perform this action"

    else:
        # Get the group
        groupList = Group.objects(name=groupName)
        if len(groupList) is 1:
            group = groupList[0]
        else:
            return "that group does not exist"

        # Get the permission
        permsList = PermissionAssignment.objects(note=note, group=group, deletedOn=None)
        if len(permsList) is 1:
            perm = permsList[0]
            perm.deletedOn = datetime.datetime.now()
            perm.save()
            return True
        else:
            return "that permission does not appear to be valid"



def canAdminGroup(user, group):
    if group.name == "public":
        return True

    groupMembershipList = GroupMembership.objects(group=group, user=user)
    if len(groupMembershipList) is not 1 or groupMembershipList[0].rank != 'A':
        return False
    else:
        return True
