from django.template.defaultfilters import slugify
from django.http import HttpResponse
from urlparse import urlparse
from shorty.helpers import *
from shorty.models import *
from shorty.forms import *
from mongoengine import *
import hashlib
import string
import random
import re


def id_generator(size=6, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


def createNoteFromForm(request, form):
    # Make sure a user was logged in
    user = getUser(request)
    if user is None:
        return HttpResponse("You are not logged in")

    # Randomly generate a URL if none was provided
    if form.cleaned_data['address'] == "":
        address = getNextRandomAddress()
    else:
        address = form.cleaned_data['address']

    # Make the address safe
    address = slugify(address)

    # Create the note
    if form.cleaned_data['doctype'] == 'L':
        l = createLink(user, address, form.cleaned_data['longAddress'])
        if l is not None:
            return '/%s/admin' % address
        else:
            # There was an error
            return "The address was already in use"
    elif form.cleaned_data['doctype'] == 'S':
        s = createSnippet(user, address, form.cleaned_data['title'])
        if s is not None:
            return '/%s' % address
        else:
            # There was an error
            return "The address was already in use"
    else:
        return 'select a valid document type'


def createLink(user, shortAddress, longAddress):
    # Check that the address is available
    noteList = Note.objects(address=shortAddress)
    if len(noteList) is not 0:
        # Address not available, return none
        return None

    l = Note(
        address=shortAddress,
        longAddress=longAddress,
        createdBy=user,
        doctype='L'
    )
    l.save()

    return l


def createSnippet(user, shortAddress, title):
    # Check that the address is available
    noteList = Note.objects(address=shortAddress)
    if len(noteList) is not 0:
        # Address not available, return none
        return None

    s = Note(
        address=shortAddress,
        title=title,
        createdBy=user,
        doctype='S'
    )
    s.save()

    return s


def createPermissionFromForm(request, form, urlCode):
    # Make sure a user was logged in
    user = getUser(request)
    if user is None:
        return "You are not logged in"

    groupName = form.cleaned_data['groupName']
    permType = form.cleaned_data['permissionLevel']

    permList = []
    if 'R' in permType:
        permList.append('R')
    if 'W' in permType:
        permList.append('W')
    if 'A' in permType:
        permList.append('A')
        permList.append('R')
        permList.append('W')

    # Get the note
    noteList = Note.objects(address=urlCode)
    if len(noteList) is 1:
        note = noteList[0]
    else:
        return "Invalid Note"

    # Check if the group already exists
    groupList = Group.objects(name=groupName)
    if len(groupList) is 1:
        group = groupList[0]
        # Check that the person creating the permission is an admin of this group
        if not canAdminGroup(user, group):
            return "No permission for this group"
    elif len(groupList) is 0:
        # Create the group
        group = createGroup(groupName, user)
    else:
        return "There was a problem with your group choice"

    # Check that this permission has not already been assigned
    permissionList = PermissionAssignment.objects(group=group, note=note, deletedOn=None)
    if len(permissionList) != 0:
        return "A permission for this group has already been created"

    p = PermissionAssignment(
        note=note,
        createdBy=user,
        group=group,
        permissions=permList
    )
    p.save()

    return True


def createGroup(name, creator):
    group = Group(
        name=slugify(name),
        createdBy=creator
    )
    group.save()

    gm = GroupMembership(
        group=group,
        user=creator,
        rank='A'
    )
    gm.save()

    return group


def addUserToGroup(form, group, creator):
    userEmail = form.cleaned_data['username']
    user = getUser(None, userEmail)
    if user is None:
        return "invalid user email"

    # Check that this user is not already added
    groupMemberships = GroupMembership.objects(group=group, user=user)
    if len(groupMemberships) is not 0:
        return "user already a member of this group"

    gm = GroupMembership(
        group=group,
        user=user,
        createdBy=creator,
        rank=form.cleaned_data['rank']
    )
    gm.save()
    return True


def newUserSignup(form):
    firstName = form.cleaned_data['firstName']
    lastName = form.cleaned_data['lastName']
    email = form.cleaned_data['email']
    password = form.cleaned_data['password']
    salt = id_generator(100)

    # Hash the password
    passwordHash = hashlib.sha512(password + salt).hexdigest()

    newUser = User(
        firstName=firstName,
        lastName=lastName,
        email=email,
        password=passwordHash,
        salt=salt
    )
    newUser.save()
    return newUser


def getNextRandomAddress():
    prevRandomAddress = RandomAddress.objects()
    if len(prevRandomAddress) is 1:
        # Use the value last saved
        prevRandomAddress = prevRandomAddress[0]
        addr = prevRandomAddress.address
    elif len(prevRandomAddress) is 0:
        # No random address has been created yet, start from 'a'
        prevRandomAddress = RandomAddress()
        addr = 'a'
    else:
        # Something got messed up, delete them all and start again
        for randAddr in prevRandomAddress:
            randAddr.delete()
        prevRandomAddress = RandomAddress()
        addr = 'a'

    # Increase this address, check that it is valid,
    # set it to the object, then return
    isValid = False
    while not isValid:
        addr = randomAddressHelper(addr)
        prevRandomAddress.address = addr

        existingNotes = Note.objects(address=addr)
        if len(existingNotes) is 0:
            isValid = True

    prevRandomAddress.save()
    return addr


def randomAddressHelper(substr):
    # If the length is one and letter is 'z', return 'aa'
    if re.search(r"^z+$", substr):
        return 'a' * (len(substr) + 1)

    # If length is one, increment the letter
    if len(substr) is 1:
        ascii = ord(substr)
        return chr(ascii + 1)

    # If all but the first character are 'z', increment first character and change rest to 'a'
    if re.search(r"^z+$", substr[1:]):
        ascii = ord(substr[:1])
        return chr(ascii + 1) + ('a' * (len(substr) - 1))

    # Else, take off the first letter and recurse
    return substr[:1] + randomAddressHelper(substr[1:])


def createApiKey(user, callbackURL):
    parsedCallback = urlparse(callbackURL)
    requestorDomain = parsedCallback.netloc

    unique = False
    while not unique:
        key = id_generator(15)
        duplicateKeys = ApiKey.objects(key=key)
        if len(duplicateKeys) is 0:
            unique = True

    newKey = ApiKey(
        user=user,
        key=key,
        owner=requestorDomain
    )
    newKey.save()
    return newKey.key
