from django.shortcuts import redirect, render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.http import HttpResponse
from shorty.helpers import *
from shorty.models import *
from shorty.forms import *
from mongoengine import *
from shorty.api import *


def authorize(request):
    user = getUser(request)

    # They just submitted the form, validate password
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            result = validateLogin(request, form)
            if result:
                callback = request.POST.get('callback', None)
                key = createApiKey(getUser(request), callback)
                return replyToCallback(callback, 'authorization_token', key)

        return render_to_response('shorty/loginDisplay.html', {
            'loginForm': form,
            'callback': form.cleaned_data['callback'],
            'error': 'incorrect username or password'
        }, context_instance=RequestContext(request))

    elif request.method == 'GET':
        callback = request.GET.get('callback', None)
        if callback is None:
            # This just won't do
            return render_to_response('shorty/errorDisplay.html', {
                'error': 'please specify a callback address'
            })

        accepted = request.GET.get('accepted', None)
        if accepted is not None:
            if accepted == 'True':
                key = createApiKey(getUser(request), callback)
                return replyToCallback(callback, 'authorization_token', key)

            elif accepted == 'False':
                # They declined permission, return to call back with the bad news
                return replyToCallback(callback, 'accepted', 'false')

        if user is None:
            # Display login form
            return render_to_response('shorty/oauthAuthorizeDisplay.html', {
                'loginForm': LoginForm(),
                'callback': callback
            }, context_instance=RequestContext(request))
        else:
            # Display request for permission
            return render_to_response('shorty/oauthAuthorizeDisplay.html', {
                'username': user,
                'callback': callback
            }, context_instance=RequestContext(request))


@csrf_exempt
def createShortLink(request, shortAddress=None):
    # Check that an access token was provided and valid
    user = checkAccessToken(request)
    if user is None:
        # They must have done something wrong
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'apikey is missing or invalid'
        })

    # Check that a long address was provided
    if 'destination' in request.POST:
        longAddress = request.POST['destination']
    else:
        # Return an error
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'please provide a desination address'
        })

    # Get a url if none was provided
    if shortAddress is None:
        shortAddress = getNextRandomAddress()

    # Create a link
    l = createLink(user, shortAddress, longAddress)
    if l is not None:
        # Link was created, display the address
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'http://qbf.tv/%s' % l.address
        })
    else:
        # Link was not created, display error
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'address already in use, link could not be created'
        })


@csrf_exempt
def setSnippetLanguage(request, shortAddress):
    # Check that an access token was provided and valid
    user = checkAccessToken(request)
    if user is None:
        # They must have done somethign wrong
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'apikey is missing or invalid'
        })

    # Get the language being set
    if 'language' in request.POST:
        language = request.POST['language']
    else:
        language = ''

    # Get the note in question
    noteList = Note.objects(address=shortAddress)
    if len(noteList) is not 1:
        # Invalid address
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'referenced note is invalid'
        })
    else:
        note = noteList[0]

    # Check that the user has permission for this note
    if canAdminNote(note, user):
        if language == '':
            note.language = None
        else:
            note.language = language
        note.save()
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'language updated'
        })
    else:
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'you do not have permission to admin this note'
        })


@csrf_exempt
def deleteGroupPermission(request, shortAddress):
    # Check that an access token was provided and valid
    user = checkAccessToken(request)
    if user is None:
        # They must have done something wrong
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'apikey is missing or invalid'
        })

    # Get the group name being deleted
    if 'groupName' in request.POST:
        groupName = request.POST['groupName']
    else:
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'group name missing in update'
        })

    # Get the note in question
    noteList = Note.objects(address=shortAddress)
    if len(noteList) is not 1:
        # Invalid address
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'referenced note is invalid'
        })
    else:
        note = noteList[0]

    # Check that the user has permission for this note
    if canAdminNote(note, user):
        response = deletePermission(note, groupName, user)
        if response is True:
            return render_to_response('shorty/ajaxMessageDisplay.html', {
                'message': 'updated'
            })
        else:
            return render_to_response('shorty/ajaxMessageDisplay.html', {
                'message': response
            })
    else:
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'you do not have permission to admin this note'
        })


@csrf_exempt
def updateNoteText(request, shortAddress):
    # Check that an access token was provided and valid
    user = checkAccessToken(request)
    if user is None:
        # They must have done something wrong
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'apikey is missing or invalid'
        })

    # Get the text being set
    if 'text' in request.POST:
        text = request.POST['text']
    else:
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'text missing in update'
        })

    # Get the note in question
    noteList = Note.objects(address=shortAddress)
    if len(noteList) is not 1:
        # Invalid address
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'referenced note is invalid'
        })
    else:
        note = noteList[0]

    # Check that the user has permission for this note
    if canSaveNote(note, user):
        note.body = text
        note.save()
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'updated'
        })
    else:
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'you do not have permission to edit this note'
        })

@csrf_exempt
def updateNoteTitle(request, shortAddress):
    # Check that an access token was provided and valid
    user = checkAccessToken(request)
    if user is None:
        # They must have done something wrong
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'apikey is missing or invalid'
        })

    # Get the title being set
    if 'title' in request.POST:
        title = request.POST['title']
    else:
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'title missing in update'
        })

    # Get the note in question
    noteList = Note.objects(address=shortAddress)
    if len(noteList) is not 1:
        # Invalid address
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'referenced note is invalid'
        })
    else:
        note = noteList[0]

    # Check that the user has permission for this note
    if canAdminNote(note, user):
        note.title = title
        note.save()
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'updated'
        })
    else:
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'you do not have permission to edit this note'
        })

@csrf_exempt
def deleteNote(request, shortAddress):
    # Check that an access token was provided and valid
    user = checkAccessToken(request)
    if user is None:
        # They must have done something wrong
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'apikey is missing or invalid'
        })

    # Get the note in question
    noteList = Note.objects(address=shortAddress)
    if len(noteList) is not 1:
        # Invalid address
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'referenced note is invalid'
        })
    else:
        note = noteList[0]

    # Check that the user has permission for this note
    if canAdminNote(note, user):
        note.deleted = datetime.datetime.now()
        note.save()
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'note deleted'
        })
    else:
        return render_to_response('shorty/ajaxMessageDisplay.html', {
            'message': 'you do not have permission to edit this note'
        })

def checkAccessToken(request):
    if 'apikey' in request.POST:
        apikey = request.POST['apikey']
        apikeylist = ApiKey.objects(key=apikey, deletedOn=None)
        if len(apikeylist) is 1:
            # The key is valid, find the user
            return apikeylist[0].user

    return None


def replyToCallback(callback, name, message):
    if '?' in callback:
        return redirect('%s&%s=%s' % (callback, name, message))
    else:
        return redirect('%s?%s=%s' % (callback, name, message))
