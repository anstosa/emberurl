from django import forms


class NewNote(forms.Form):
    doctype = forms.ChoiceField(widget=forms.RadioSelect, choices=[('L', 'link'), ('S', 'snippet')], label='', required=True)
    address = forms.CharField(label='qbf.tv/', required=False)
    longAddress = forms.CharField(label='link to', required=False)
    title = forms.CharField(label='title', required=False)
    expiration = forms.ChoiceField(choices=[('-1', 'never'), ('1', '1 day'), ('7', '1 week'), ('30', '1 month')], label='exp', required=False)


class LoginForm(forms.Form):
    email = forms.EmailField(label='Email')
    password = forms.CharField(label='Password', widget=forms.PasswordInput(render_value=False))


class RegistrationForm(forms.Form):
    firstName = forms.CharField(label='First Name', required=True)
    lastName = forms.CharField(label='Last Name', required=True)
    email = forms.EmailField(label='Email Address', required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput(render_value=False))


class NewPermission(forms.Form):
    groupName = forms.CharField(label='Group Name', required=True)
    permissionLevel = forms.ChoiceField(choices=[('R', 'Read'), ('RW', 'Read & Write'), ('A', 'Admin')], label='Permission Level', required=True)


class NewGroupMember(forms.Form):
    username = forms.CharField(label='User Email', required=True)
    rank = forms.ChoiceField(choices=[('M', 'Member'), ('A', 'Admin')], label='Permission Level', required=True)
