from django.shortcuts import redirect, render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.http import HttpResponse
from shorty.helpers import *
from shorty.models import *
from shorty.forms import *
from mongoengine import *
from shorty.api import *


def index(request):
    if request.method == 'POST':
        newNote = NewNote(request.POST)
        error = False
        if newNote.is_valid():
            # Check that the address is unique
            existingNotes = Note.objects(address=newNote.cleaned_data['address'])
            if len(existingNotes) is 0:
                result = createNoteFromForm(request, newNote)
                if result[:1] == '/':
                    return redirect(result)
                else:
                    error = result
            else:
                error = 'Address already in use'

        # Display the error if there was one
        if error is not False:
            return render_to_response('shorty/createNote.html', {
                'newNote': newNote,
                'username': getUser(request),
                'error': error
            }, context_instance=RequestContext(request))
    else:
        subdomain = getSubdomain(request)
        if subdomain is not None:
            return redirect('http://qbf.tv/%s' % subdomain)
        else:
            user = getUser(request)
            if user is not None:
                return render_to_response('shorty/createNote.html', {
                    'newNote': NewNote(),
                    'username': user,
                }, context_instance=RequestContext(request))
            else:
                return render_to_response('shorty/errorDisplay.html', {
                    'error': 'please log in to create a new note'
                })


def about(request):
    return render_to_response('shorty/errorDisplay.html', {
        'error': 'the about page has not been made yet, sorry'
    })


def admin(request, urlCode):
    return viewURLAdmin(urlCode, request)


def login(request):
    # Check that the user is not logged in already
    if getUser(request) is not None:
        return redirect('/')

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            result = validateLogin(request, form)
            if result:
                # Send the user on their way
                return redirect("/")

        # Do not display registration form since they indicated they wanted to log in
        return render_to_response('shorty/loginDisplay.html', {
            'loginForm': form,
            'error': 'incorrect username or password'
        }, context_instance=RequestContext(request))
    else:
        return render_to_response('shorty/loginDisplay.html', {
            'loginForm': LoginForm(),
            'registerForm': RegistrationForm()
        }, context_instance=RequestContext(request))


def register(request):
    # Check that the user is not logged in already
    if getUser(request) is not None:
        return redirect('/')

    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = newUserSignup(form)
            request.session['userEmail'] = user.email
            return redirect('/')
        else:
            # Do not display login form since they indicated they wanted to register
            return render_to_response('shorty/loginDisplay.html', {
                'registrationForm': form
            }, context_instance=RequestContext(request))
    else:
        return render_to_response('shorty/loginDisplay.html', {
            'loginForm': LoginForm(),
            'registerForm': RegistrationForm()
        }, context_instance=RequestContext(request))


def logout(request):
    if 'userEmail' in request.session:
        del request.session['userEmail']
    return redirect('/')


def url(request, urlCode):
    subdomain = getSubdomain(request)
    if subdomain is not None:
        return redirect("/invalid/%s/%s" % (urlCode, subdomain))

    return viewURL(urlCode, request)


def groupAdmin(request, groupName):
    # Get the user
    user = getUser(request)
    if user is None:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'Please log in to view groups'
        })

    # Find the group in question
    groupList = Group.objects(name=groupName)
    if len(groupList) is 1:
        group = groupList[0]
    else:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'No group with that name exists',
            'username': user.email
        })

    if request.method == 'POST':
        form = NewGroupMember(request.POST)
        if form.is_valid():
            error = False
            result = addUserToGroup(form, group, user)
            if result is True:
                return render_to_response('shorty/groupAdmin.html', {
                    'group': group,
                    'currentMembers': GroupMembership.objects(group=group),
                    'username': user,
                    'message': 'user added to group',
                    'newMember': form
                }, context_instance=RequestContext(request))
            else:
                error = result
        else:
            error = 'invalid form'

        if error is not False:
            return render_to_response('shorty/groupAdmin.html', {
                'group': group,
                'currentMembers': GroupMembership.objects(group=group),
                'username': user,
                'error': error,
                'newMember': form
            }, context_instance=RequestContext(request))
    else:
        # Check that this user is an admin for that group
        memberships = GroupMembership.objects(group=group, user=user, rank='A')
        if len(memberships) is 1:
            return render_to_response('shorty/groupAdmin.html', {
                'group': group,
                'currentMembers': GroupMembership.objects(group=group),
                'username': user,
                'newMember': NewGroupMember()
            }, context_instance=RequestContext(request))
        else:
            return render_to_response('shorty/errorDisplay.html', {
                'error': 'You do not have administrative privledges for this group',
                'username': user
            })


def mySnippets(request):
    # Get the user
    user = getUser(request)
    if user is None:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'Please log in to view groups'
        })

    # Get API Key
    apikeyList = ApiKey.objects(user=user, owner="qbf.tv", deleted=None)
    if len(apikeyList) is 1:
        apikey = apikeyList[0].key
    else:
        apikey = createApiKey(user, 'http://qbf.tv')

    # Get the snippets
    snippetList = Note.objects(createdBy=user, deleted=None, doctype='S')

    return render_to_response('shorty/myPage.html', {
        'page': 'snippet',
        'apikey': apikey,
        'snippets': snippetList,
        'username': user
    })


def myLinks(request):
    # Get the user
    user = getUser(request)
    if user is None:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'Please log in to view groups'
        })

    # Get API Key
    apikeyList = ApiKey.objects(user=user, owner="qbf.tv", deleted=None)
    if len(apikeyList) is 1:
        apikey = apikeyList[0].key
    else:
        apikey = createApiKey(user, 'http://qbf.tv')

    # Get the links
    linkList = Note.objects(createdBy=user, deleted=None, doctype='L')

    return render_to_response('shorty/myPage.html', {
        'page': 'link',
        'apikey': apikey,
        'links': linkList,
        'username': user
    })


def myGroups(request):
    # Get the user
    user = getUser(request)
    if user is None:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'Please log in to view groups'
        })
    
    # Get API Key
    apikeyList = ApiKey.objects(user=user, owner="qbf.tv", deleted=None)
    if len(apikeyList) is 1:
        apikey = apikeyList[0].key
    else:
        apikey = createApiKey(user, 'http://qbf.tv')

    return render_to_response('shorty/myPage.html', {
        'page': 'group',
        'apikey': apikey,
        'username': user
    })


def invalid(request, urlCode1, urlCode2):
    if urlCode2 is not None:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'please only request one url at a time'
        })
    else:
        return render_to_response('shorty/errorDisplay.html', {
            'error': 'the requested URL is invalid'
        })
