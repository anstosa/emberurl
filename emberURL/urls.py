from django.conf.urls import patterns, url

urlpatterns = patterns('',
    ### Main
    url(r'^/?$', 'shorty.views.index'),  # Homepage for creating new items, searching, or admin
    url(r'^invalid/(?P<urlCode1>[\d\w]+)/(?P<urlCode2>[\d\w]+)?/?$', 'shorty.views.invalid'),  # The URLs you seek are different or invalid
    url(r'^about/?$', 'shorty.views.about'),  # About Screen

    ### User Management
    url(r'^login/?$', 'shorty.views.login'),  # Login Screen
    url(r'^logout/?$', 'shorty.views.logout'),  # Logout Screen
    url(r'^register/?$', 'shorty.views.register'),  # New User Creation
    url(r'^group/(?P<groupName>[\d\w\-_]+)/?$', 'shorty.views.groupAdmin'),  # Group Administration
    url(r'^myLinks/?$', 'shorty.views.myLinks'),  # Display all links created by this user
    url(r'^mySnippets/?$', 'shorty.views.mySnippets'),  # Display all snippets created by this user
    url(r'^myGroups/?$', 'shorty.views.myGroups'),  # Display all groups created by this user

    ### OAuth
    url(r'^oauth/authorize/?$', 'shorty.api.authorize'),  # Users authorize a site

    ### API
    url(r'^api/short_link/(?P<shortAddress>[\d\w]+)?/?$', 'shorty.api.createShortLink'),  # Create a short link using API
    url(r'^api/(?P<shortAddress>[\d\w]+)/lang/?$', 'shorty.api.setSnippetLanguage'),  # Update snippet language
    url(r'^api/(?P<shortAddress>[\d\w]+)/body/?$', 'shorty.api.updateNoteText'),  # Update snippet body
    url(r'^api/(?P<shortAddress>[\d\w]+)/deleteGroupPerm/?$', 'shorty.api.deleteGroupPermission'),  # Remove permission from note
    url(r'^api/(?P<shortAddress>[\d\w]+)/title/?$', 'shorty.api.updateNoteTitle'),  # Update the title of a note
    url(r'^api/(?P<shortAddress>[\d\w]+)/delete/?$', 'shorty.api.deleteNote'),  # Mark a note as deleted

    ### URLs
    url(r'^(?P<urlCode>[\d\w]+)/admin/?$', 'shorty.views.admin'),  # URL Admin Screen
    url(r'^(?P<urlCode>[\d\w]+)/?$', 'shorty.views.url'),  # Evaluate and View URL
)
