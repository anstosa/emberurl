function checkHidden () {
    if ($('#id_doctype_1').prop('checked')) {
        $('#id_longAddress').parent().hide();
        $('#id_title').parent().show();
    } else if ($('#id_doctype_0').prop('checked')){
        $('#id_longAddress').parent().show();
        $('#id_title').parent().hide();
    }
}

$(function() {
    $('input[type="radio"]').on('change', function() {
        $(this)
            .closest('li').addClass('highlight')
            .siblings().removeClass('highlight')
        ;
    });

    $("#id_address").parent().children(":first").text("qbf.tv/")

    $('#id_doctype_0')
        .prop('checked', true)
        .closest('li').addClass('highlight')
    ;

    $('#id_address').attr('placeholder', 'Randomly Generated');

    $('#id_doctype_0, #id_doctype_1').on('change', checkHidden);
    checkHidden();
});
